function linearInterp(curve, testX) {
  for (let i = 0; i < curve.length - 1; i++) {
    if (curve[i][0] <= testX && testX <= curve[i + 1][0]) {
      let slope = (curve[i + 1][1] - curve[i][1]) /
          (curve[i + 1][0] - curve[i][0]);
      let yIntercept = curve[i][1] - (slope * curve[i][0]);
      return (slope * testX) + yIntercept;
    }
  }
  console.error("invalid linearInterp arguments", curve, testX);
  return NaN;
}

function pointUnderCurve(curve, point) {
  return linearInterp(curve, point[0]) > point[1];
}

function randomFloat(min, max) {
  return (RNG() * (max - min)) + min;
}

// assumes weights are sorted by X and there are more than 1 weight
function weightedRand(weights) {
  let xMin = weights[0][0];
  let xMax = weights[weights.length - 1][0];
  let yMin = 0;
  let ys = [];
  for (let i = 0; i < weights.length; i++) {
    ys.push(weights[i][1]);
  }
  let yMax = Math.max(...ys);
  let attemptCount = 0;
  while (attemptCount < 10000) {
    let sample = [randomFloat(xMin, xMax), randomFloat(yMin, yMax)];
    if (pointUnderCurve(weights, sample)) {
      return sample[0];
    }
  }
  console.error("failed to calculate weightedRand value, using fallback");
  return weights[0][0];
}

// assumes there is more than 1 weight
function weightedChoice(weights) {
  let probSum = 0;
  for (let i = 0; i < weights.length; i++) {
    probSum += weights[i][1];
  }
  let pick = randomFloat(0, probSum);
  let currentPos = 0;
  for (let i = 0; i < weights.length; i++) {
    currentPos += weights[i][1];
    if (currentPos >= pick) {
      return weights[i][0];
    }
  }
  console.error("failed to calculate weightedChoice value, using fallback");
  return weights[0][0];
}

const STANZA_WIDTH_PCT_WEIGHTS = [[30, 0], [60, 10], [80, 0]];
const STANZA_INDENT_PCT_WEIGHTS = [[0, 10], [30, 2], [70, 0]];
const ACTUAL_WIDTH_PCT_ALLOW_JUSTIFY = 30;
// Wider-stanza mobile variations for windows smaller than 600px
const STANZA_WIDTH_PCT_WEIGHTS_MOBILE = [[40, 0], [65, 1], [80, 10], [100, 0]];
const STANZA_INDENT_PCT_WEIGHTS_MOBILE = [[0, 10], [30, 2]];
const ACTUAL_WIDTH_PCT_ALLOW_JUSTIFY_MOBILE = 60;


function applyInlineStyleForStanza(domElement, docWidth) {
  // need to account for fact that on mobile much wider pcts are needed
  let stanzaWidthPctWeights, stanzaIndentPctWeights, actualWidthPctAllowJustify;
  if (docWidth > 600) {
    stanzaWidthPctWeights = STANZA_WIDTH_PCT_WEIGHTS;
    stanzaIndentPctWeights = STANZA_INDENT_PCT_WEIGHTS;
    actualWidthPctAllowJustify = ACTUAL_WIDTH_PCT_ALLOW_JUSTIFY;
  } else {
    stanzaWidthPctWeights = STANZA_WIDTH_PCT_WEIGHTS_MOBILE;
    stanzaIndentPctWeights = STANZA_INDENT_PCT_WEIGHTS_MOBILE;
    actualWidthPctAllowJustify = ACTUAL_WIDTH_PCT_ALLOW_JUSTIFY_MOBILE;
  }
  let widthPct = weightedRand(stanzaWidthPctWeights);
  let marginLeftPct = weightedRand(stanzaIndentPctWeights);
  let actualWidthPct = Math.min(widthPct, 100 - marginLeftPct);
  let alignmentWeights = [
    ['left', 100], 
    ['justify', actualWidthPct < actualWidthPctAllowJustify ? 0 : 50]];
  let alignment = weightedChoice(alignmentWeights);
  domElement.style.maxWidth = '' + widthPct + '%';
  domElement.style.marginLeft = '' + marginLeftPct + '%';
  domElement.style.textAlignment = alignment;
}

function formatDocument() {
  let docWidth = window.innerWidth ||
      document.documentElement.clientWidth || document.body.clientWidth;
  let poemElements = document.querySelectorAll('.poem > p');
  for (let i = 0; i < poemElements.length; i++) {
    applyInlineStyleForStanza(poemElements[i], docWidth);
  }
}

