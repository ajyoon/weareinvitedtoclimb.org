# We Are Invited to Climb

This is the source code for the online digital edition of [_We Are Invited to Climb_](https://awst-press.com/shop/we-are-invited-to-climb), a book of chance poetry by [Andrew Yoon](https://andrewyoon.art) published September 2021 by [Awst Press](https://awst-press.com/).

The poetry collection is written in [BML](https://bml-lang.org), a poetry programming language which incorporates carefully composed randomized elements into linear text. On each generation of the collection, hundreds of poetic elements—words, phrases, line breaks, layout—are chosen by chance. The printed version is one such iteration among virtually infinite possibilities, but the book's accompanying website at https://weareinvitedtoclimb.org allows readers to generate, explore, and share other unique realizations. This repository is the source code the that website, containing by extension the source code of the text.

If you find this project interesting, please consider supporting the author by [buying a print copy of the book here](https://awst-press.com/shop/we-are-invited-to-climb).

## Technical Overview

The text's BML source code lives at [`/public/content.bml`](/public/content.bml). To learn more about the BML language, visit [bml-lang.org](https://bml-lang.org). Chance-determined formatting is performed in [`/public/js/formatting.js`](/public/js/formatting.js) using methods closely approximating those used for the printed book's formatting.

The website is built using the static site generator [Zola](https://www.getzola.org/) and is hosted on Netlify. Its frontend dependencies are [`seedrandom`](https://github.com/davidbau/seedrandom) (for reproducible chance-determined formatting) and [`tippy`](https://atomiks.github.io/tippyjs/)+[`popper`](https://popper.js.org/) (for poem title link menus). The stylesheet is written in [SCSS](https://sass-lang.com/).

## Credits

_We Are Invited to Climb_ was published by [Awst Press](https://awst-press.com/).
- Editing by [Tatiana Ryckman](http://www.tatianaryckman.com/)
- Copyediting by David McNamara
- Book and cover design by [LK James](https://www.lkjames.com)
- Online edition by [Andrew Yoon](https://andrewyoon.art)
- Book interior cover image (Geumgang Jeondo) and online title image (Inwang Jesaekdo) by [Jeong Seon](https://en.wikipedia.org/wiki/Jeong_Seon)

## License

Both the print book and this digital edition repository are published under a Creative Commons BY-NC-SA license. See [LICENSE.txt](/LICENSE.txt) for complete terms.

